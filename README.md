# usteer-web-ucode

Experimental ucode based web app for interacting with usteer.

This app is mostly a playground to experiment with ucode.
It queries the local usteer daemon and constructs a list of ESSes, with all
BSSes in each ESS, and all clients connected to each BSS. If the BSS is local
to the AP running usteer-web-ucode, there will be a kick and transition button
for each client. The transition button will be disabled for clients not
supporting 802.11v, or when there are no other APs running usteerd configured
for the same SSID.

## Requirements

This app requires a running [usteer daemon](https://git.openwrt.org/?p=project/usteer.git).
To use the BSS Transitioning features, you'll need add least two APs running
usteer in the same L2 broadcast domain, configured with the same SSID.

Aside from that, it requires uhttpd with ucode plugin support. For OpenWrt this
translates to OpenWrt 22.03 or later. For OpenWiFi, use ApNos 2.7.0 or later.

## Installing

### Feed

This repository can be configured as an [OpenWrt feed](https://openwrt.org/docs/guide-developer/feeds):

```
echo "src-git usteer_web_ucode https://codeberg.org/stintel/usteer-web-ucode.git" >> feeds.conf
./scripts/feeds update
./scripts/feeds install usteer-web-ucode
```

You can now select it in menuconfig like any other package.

### Manual install

As this is an architecture independent package, the ipk can be installed on any
device, as long as the OpenWrt version is new enough to have all the deps.

For convenience, here's an [ipk](https://codeberg.org/api/packages/stintel/generic/usteer-web-ucode/0.0.0/usteer-web-ucode_0.0.0-1_all.ipk).

To install the ipk on a device, simply run these commands on that device:
```
uclient-fetch -P /tmp/ https://codeberg.org/api/packages/stintel/generic/usteer-web-ucode/0.0.0/usteer-web-ucode_0.0.0-1_all.ipk
opkg install /tmp/usteer-web-ucode_0.0.0-1_all.ipk
```

## Configuring uhttpd

Configure the following ucode prefix in uhttpd:
`/usteer=/usr/share/ucode/uhttpd/usteer.uc`
Restart uhttpd afterwards, and open http://openwrt/usteer.html.

```
uci add_list uhttpd.main.ucode_prefix='/usteer=/usr/share/ucode/uhttpd/usteer.uc'
uci commit
/etc/init.d/uhttpd restart
```
