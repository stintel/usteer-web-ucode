{%
/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * Copyright (C) 2022  Stijn Tintel <stijn@linux-ipv6.be>
 */

'use strict';

function error(msg) {
	print('Content-Type: text/plain\n\n');
	printf("%s\n", msg);
	exit(0);
};

function get_args(data) {
	let args = split(data, '&');
	let argsObj = {};

	for (let arg in args) {
		let tmp = split(arg, '=');
		argsObj[tmp[0]] = tmp[1];
	}

	return argsObj;
}

function home() {
	print('Content-Type: text/html\n\n');
	print('<a href="/usteer.html">usteer dashboard</a>');
};

function print_json_header() {
	print('Content-Type: application/json\n\n');
};

function send_bss_transition_request(data) {
	const ubus = require('ubus');
	const args = get_args(data);

	let ubus_conn = ubus.connect();
	if (ubus_conn.list(args.node)) {

		const ubus_data = {
				abridged: args.abridged,
				addr: args.client,
				cell_pref: args.cellPref,
				disassociation_imminent: args.disassociation_imminent,
				disassociation_timer: args.disassociation_timer,
				mbo_reason: args.mbo_reason,
				neighbors: split(args.neighbors, ','),
				reassoc_delay: args.reassoc_delay,
				validity_period: args.validity_period,
		};

		ubus_conn.call(args.node, "bss_transition_request", ubus_data);

		return `{ "msg": "Sent BSS Transition Request", "args": ${args} }`;
	} else {
		return `{ "msg": "${args.node} not found in ubus" }`;
	}
}


function send_kick(data) {
	const ubus = require('ubus');
	const args = get_args(data);

	let ubus_conn = ubus.connect();
	if (ubus_conn.list(args.node)) {

		const ubus_data = {
				addr: args.client,
				reason: args.reason_code,
		};

		ubus_conn.call(args.node, "del_client", ubus_data);

		return `{ "msg": "Kicked client", "args": ${args} }`;
	} else {
		return `{ "msg": "${args.node} not found in ubus" }`;
	}
}

global.handle_request = function(env) {
	const ubus = require('usteer.ubus');

	if (!ubus)
		error('usteer not available in ubus');

	switch (match(env.PATH_INFO, /^\/([^\/]+)/)?.[1]) {
		case 'get_clients':
			print_json_header();
			print(ubus.get_clients());
			break;
		case 'get_connected_clients':
			print_json_header();
			print(ubus.get_connected_clients());
			break;
		case 'get_local_info':
			print_json_header();
			print(ubus.get_local_info());
			break;
		case 'get_remote_info':
			print_json_header();
			print(ubus.get_remote_info());
			break;
		case 'send_bss_transition_request':
			print_json_header();
			print(send_bss_transition_request(env.QUERY_STRING));
			break;
		case 'send_kick':
			print_json_header();
			print(send_kick(env.QUERY_STRING));
			break;
		default:
			home();
	}
};
