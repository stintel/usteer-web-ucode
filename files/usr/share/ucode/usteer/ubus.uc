/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * Copyright (C) 2022  Stijn Tintel <stijn@linux-ipv6.be>
 */

'use strict';

const ubus = require('ubus');

let ubus_conn;

return {
	get_clients: function() {
		return this.ubus_call('get_clients');
	},

	get_config: function() {
		return this.ubus_call('get_config');
	},

	get_connected_clients: function() {
		return this.ubus_call('connected_clients');
	},

	get_local_info: function() {
		return this.ubus_call('local_info');
	},

	get_remote_hosts: function() {
		return this.ubus_call('remote_hosts');
	},

	get_remote_info: function() {
		return this.ubus_call('remote_info');
	},

	ubus_call: function(method, args) {
		if (this.ubus_connect()) {
			return ubus_conn.call('usteer', method, args);
		}
	},

	ubus_connect: function() {
		/* Nullish coalescing operator */
		ubus_conn ??= ubus.connect();

		return(ubus_conn != null);
	},
};
