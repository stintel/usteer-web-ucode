'use strict';

const bss = {
	bssid: null,
	clients: [],
	freq: null,
	node: null,
	nr: null,
	remote: false
};

const ess = {
	bss: []
};

const sta = {
	btm: null,
	mac: null,
	mbo: null,
	signal: null
};

async function callUsteer(url){
	try {
		let response = await fetch(decodeURIComponent(url));
		return await response.json();
 
	 } catch (error) {
		 console.error(error);
	 }
}

async function getBssesFromNodeInfo(nodeInfo, isRemote = false) {
	let bsses = []

	const clients = await getClients();

	Object.keys(nodeInfo).forEach(node => {
		let b = Object.create(bss);
		const ssid = getSsidFromNodeInfo(nodeInfo[node]);

		b.bssid = nodeInfo[node].bssid;
		b.clients = getClientsForNode(clients, node);
		b.freq = nodeInfo[node].freq;
		b.node = node;
		b.nr = getNeighborReportElementFromNodeInfo(nodeInfo[node]);
		b.remote = isRemote;
		b.ssid = ssid;

		bsses.push(b);
	});

	return bsses;
}

async function getClients() {
	return await(callUsteer('/usteer/get_clients'));
}

function getClientsForNode(clients, node) {
	let nodeClients = [];

	Object.keys(clients).forEach(client => {
		if (clients[client][node]?.connected === true) {
			let c = Object.create(sta);

			c.mac = client;
			c.signal = clients[client][node].signal;

			nodeClients.push(c);
		}
	});

	return nodeClients;
}

async function getConnectedClients() {
	return await(callUsteer('/usteer/get_connected_clients'));
}

async function getEsses() {
	let esses = {};

	const localInfo = await getLocalInfo();
	const localBsses = await getBssesFromNodeInfo(localInfo);

	const remoteInfo = await getRemoteInfo();
	const remoteBsses = await getBssesFromNodeInfo(remoteInfo, true);

	const bsses = localBsses.concat(remoteBsses);

	for (let i in bsses) {
		let ssid = bsses[i].ssid;
		// we use the ssid as key in the esses object so keeping it in the bss object is redundant
		delete(bsses[i].ssid);

		if (!esses.hasOwnProperty(ssid)) {
			let es = Object.create(ess);
			es.bss = [bsses[i]];

			// why does this have to be so complicated ???
			let e = {
				[ssid]: es
			}

			Object.assign(esses, e);
		} else {
			esses[ssid].bss.push(bsses[i]);
		}
	}

	const connectedClients = await getConnectedClients();

	Object.keys(connectedClients).forEach(node => {
		Object.keys(esses).forEach(ess => {
			esses[ess].bss.forEach((bss, bssIndex) => {
				if (bss.node === node) {
					esses[ess].bss[bssIndex].clients.forEach((client, clientIndex) => {
						esses[ess].bss[bssIndex].clients[clientIndex].btm = connectedClients[node][client.mac]['bss-transition-management'];
						esses[ess].bss[bssIndex].clients[clientIndex].mbo = connectedClients[node][client.mac]['multi-band-operation'];
					})
				}
			})
		})
	})

	return esses;
}

async function getLocalInfo() {
	return await(callUsteer('/usteer/get_local_info'));
}

function getNeighborReportElementsForEss(ess, own) {
	let elements = [];

	ess.bss.forEach((bss) => {
		if (bss.nr !== own) {
			elements.push(bss.nr);
		}
	})

	return elements;
}

function getNeighborReportElementFromNodeInfo(nodeInfo) {
	return nodeInfo["rrm_nr"][2];
}

async function getRemoteInfo() {
	return await(callUsteer('/usteer/get_remote_info'));
}

function getSsidFromNodeInfo(nodeInfo) {
	return nodeInfo["rrm_nr"][1];
}

function renderKickButton(ess, bss, client) {
	return `<button onclick="sendKick('${bss.node}','${client.mac}')">Kick</button>`;
}

function renderTransitionButton(ess, bss, client) {
	let btm = false;
	let disabled = 'disabled';
	let mbo = false;
	let neighborElements = getNeighborReportElementsForEss(ess, bss.nr);

	bss.clients.forEach(c => {
		if (c.mac == client.mac) {
			btm = c.btm;
			mbo = c.mbo;
			return;
		}
	})

	if (btm === true  && neighborElements.length > 0) {
		disabled = '';
	}

	return `<button ${disabled} onclick="startBssTransitionRequest('${bss.node}','${client.mac}','${neighborElements}','${mbo}')">Transition</button>`;
}

async function renderEsses() {
	const esses = await getEsses();

	let container = document.getElementById('esses');
	let numClients = 0;

	let html = '<table>';

	html += '<thead>';
	html += '<tr><td>SSID</td><td>BSSID</td><td>Frequency</td><td>Client</td><td>Signal</td><td></td>';
	html += '</thead>';

	html += '<tbody>';

	Object.keys(esses).forEach((ssid) => {
		html += '<tr>';
		html += '<td>' + ssid + '</td>';
		esses[ssid].bss.forEach((bss, bssIndex) => {
			if (bssIndex > 0) {
				html += '<td></td>';
			}
			html += '<td>' + bss.bssid + '</td>';
			html += '<td>' + bss.freq + ' MHz</td>';

			if (Array.isArray(bss.clients)) {
				if (bss.clients.length > 0) {
					bss.clients.forEach((client, clientIndex) => {
						numClients += 1;
						if (clientIndex > 0) {
							html += '<td></td><td></td><td></td>';
						}
						html += '<td>' + client.mac + '</td>';
						html += '<td>' + client.signal + ' dBm </td>';
						html += '<td>';

						if (bss.remote === false) {
							html += renderKickButton(esses[ssid], bss, client);
							html += renderTransitionButton(esses[ssid], bss, client);
						}

						html += '</td>';
						html += '</tr>';
					})
				} else {
					html += '<td></td>';
					html += '</tr>';
				}
			}
			html += '</tr>';
		})
		html += '</tr>';
	})

	html += '</tbody>'
	html += '</table>';
	html += '<br>';
	html += `Total clients: ${numClients}`;

	container.innerHTML = html;
}

function startBssTransitionRequest(node, client, neighbors, mbo) {
	const bssTransitionRequest = document.getElementById('bssTransitionRequest');
	const dialogConfirmButton = document.getElementById('dialogConfirmButton');
	const mboDialog = document.getElementById('mboDialog');

	bssTransitionRequest.showModal();

	if (mbo == "true")
		mboDialog.hidden = false;

	dialogConfirmButton.addEventListener('click', sendBssTransitionRequest);

	dialogConfirmButton.client = client;
	dialogConfirmButton.neighbors = neighbors;
	dialogConfirmButton.node = node;

	bssTransitionRequest.addEventListener('close', (e) => {
		mboDialog.hidden = true;
	})
}

async function sendBssTransitionRequest(event) {
	const bssTransitionRequest = document.getElementById('bssTransitionRequest');
	const params = new URLSearchParams({
		abridged: document.getElementById('aBridged').checked,
		cell_pref: document.getElementById('cellPref').value,
		client: event.currentTarget.client,
		disassociation_imminent: document.getElementById('disassociationImminent').checked,
		disassociation_timer: document.getElementById('disassociationTimer').value,
		mbo_reason: document.getElementById('mboReason').value,
		node: event.currentTarget.node,
		neighbors: event.currentTarget.neighbors,
		reassoc_delay: document.getElementById('reassocDelay').value,
		// 802.11v spec users interval, hostapd ubus method uses period
		validity_period: document.getElementById('validityInterval').value,
	})

	dialogConfirmButton.removeEventListener('click', sendBssTransitionRequest);

	let resp = await callUsteer(`/usteer/send_bss_transition_request?${params}`);

	alert(JSON.stringify(resp));
}

async function sendKick(node, client) {
	// default 802.11 Reason Code WLAN_REASON_DISASSOC_AP_BUSY
	let reasonCode = parseInt(prompt("802.11 Reason Code?", 5));

	if (reasonCode >= 0 && reasonCode <= 65535) {
		const params = new URLSearchParams({
			client: client,
			node: node,
			reason_code: reasonCode,
		})

		let resp = await callUsteer(`/usteer/send_kick?${params}`);

		alert(JSON.stringify(resp));
	} else {
		alert("Invalid 802.11 Reason Code!");
	}
}
